cmake_minimum_required(VERSION 3.2)
project(optsim)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14  -W -Wall -Werror")


find_package( Boost 1.54 COMPONENTS program_options log filesystem iostreams thread system REQUIRED )
if (NOT Boost_FOUND)
    message(FATAL_ERROR "Could not find Boost!")
endif()

include_directories( ${Boost_INCLUDE_DIR} )


set(SOURCE_FILES
        main.cc
        trace_reader.cc trace_reader.hh
        fa_opt_cache.cc fa_opt_cache.hh
        )

add_executable(optsim ${SOURCE_FILES})

target_link_libraries(optsim ${Boost_LIBRARIES})
