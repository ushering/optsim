//
// Created by Ruisheng Wang on 2/28/16.
//

#ifndef __TRACEREADER_HH__
#define __TRACEREADER_HH__



#include <boost/iostreams/filtering_stream.hpp>

#include <cstdint>
#include <fstream>

struct Request {
  uint64_t cycle;
  uint64_t tag;
  uint64_t pc;
  uint64_t instSeqId;
  uint64_t acsSeqId;
  double nextRef;
};


class TraceReader {
public:
  TraceReader(std::string trace_file_path);

  bool read(Request & req);

private:

  std::ifstream m_trace_infile;
  boost::iostreams::filtering_istream m_trace_stream;

};


#endif // __TRACEREADER_HH__
