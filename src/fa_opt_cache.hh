#ifndef __FA_OPT_CACHE_HH__
#define __FA_OPT_CACHE_HH__

#include <iostream>
#include <map>
#include <vector>
#include <queue>
#include <list>

typedef std::map<uint64_t, uint64_t, std::greater<uint64_t> > Group;  // key: priority,value: tag, group is sorted by the descending order of priority

struct Entry {
  uint64_t tag;
  uint64_t priority;
};

class FAOPTCache {
 public:

  bool access(uint64_t tag, double next_ref);
  void outputMisses(std::ostream & out);
  
 private:
  uint64_t total_accesses_{0};
  std::map<uint64_t, uint64_t> hits_;
  Entry top_entry_{UINT64_MAX, UINT64_MAX};
  std::list<Group > stack_;
  
};
 
#endif  // __FA_OPT_CACHE_H__
