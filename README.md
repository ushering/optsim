# What is it

A program that simulate a fully associative cache under OPT replacement policy.

This program implements *Stack processing with grouping* algorithm that is proposed by Sugumar et al., in ["Efficient Simulation of Caches Under Optimal Replacement with Applications to Miss Characterization"](http://dx.doi.org/10.1145/166962.166974)

# How to build

## Required package

- libboost > 1.55
- g++ > 5
- cmake > 3.2

## Build commands

```
$ mkdir build
$ cd build
$ cmake ../src
$ make

```

Executable `optsim` will be generated inside build directory.

# How to use it

- Download cache access trace files of [SPEC CPU2006 Benchmark Suites](https://www.spec.org/cpu2006/) from [here](http://pan.baidu.com/s/1hqSzGag). Please visit [this project](https://bitbucket.org/ushering/snipersim_trace) to see how the cache traces are generated. 

- Command to run the simulation

    ```
    $ ./build/optsim -b your_benchmark_name -t your_trace_dir_path -o your_output_dir_path
    ```

- Here is an example to to run the simulation

    ```
    $ ./build/optsim -b cactusADM -t cpu2006_l3traces -o
    ```

- A python script `gen_data.py` is provided to simulate all benchmarks in the trace directory

    ```
    $ python3 gen_data.py -e build/optsim -t ../cpu2006_l3traces -o outputs
    ```

- Check the output
    - the output file (e.g, `cactus_urd_opt_miss_curve.csv`) is a csv file which record the number misses at each cache size (aka, unique reuse distance)
    - For example, the following data means that there are 4257113 misses for a fully associative cache with size of 8192 cache lines under OPT replacement policy.

        | num_lines | num_misses |
        |-----------------------|------------|
        | ...                   | ...        |
        | 8196                  | 4257113    |
        | ...                   | ...        |