#include "fa_opt_cache.hh"
#include <limits>
#include <boost/lexical_cast.hpp>

using namespace std;


bool
FAOPTCache::access(uint64_t tag, double next_ref) { // note that smaller value means a higher priority
  
  total_accesses_++;

  uint64_t priority = total_accesses_;   // make priority unique

  if (next_ref != std::numeric_limits<double>::infinity()) {
    priority = UINT64_MAX - boost::lexical_cast<uint64_t>(next_ref);  // larger timestamp of next reference, smaller priority
  }
  
  if (top_entry_.tag == UINT64_MAX) { // empty stack
    top_entry_.tag = tag;
    top_entry_.priority = priority;
    return false;  // cold miss
  }

  // the following code is based on Algorithm 4 (Stack processing with grouping) in Sugumar et al. paper
  // "Efficient Simulation of Caches Under Optimal Replacement with
  // Applications to Miss Characterization "


  if (top_entry_.tag == tag) {  // hit at the top entry
    // update priority of top of stack and record hit
    hits_[0]++;
    top_entry_.priority = priority;
    return true;
  }


  // delete first stack entry and make it de
  // insert address at the top of the stack
  Entry displaced_entry{top_entry_.tag, top_entry_.priority};  // copy
  top_entry_.tag = tag;
  top_entry_.priority = priority;

  auto it = begin(stack_);
  uint64_t distance = 1;

  while (it != end(stack_)) {

    auto & group = (*it);
    assert(group.size() > 0);
    auto & head_entry = *begin(group);

    if (head_entry.second == tag) {  // hit at the top of group


      hits_[distance]++;

      // delete top of group
      group.erase(group.begin());

      if (it == begin(stack_)) {

        // create a new group
        stack_.push_front(Group());

        //put de in the new group
        auto & first_group = stack_.front();
        first_group[displaced_entry.priority] = displaced_entry.tag;  // insert entry

      } else {

        // add de to end of previous group
        Group & prev_group = *prev(it);
        prev_group[displaced_entry.priority] = displaced_entry.tag;  // insert entry
      }

      if (group.size() == 0) {  // empty group
        // delete empty group from the stack_
        stack_.erase(it);
      }

      return true;

    } else {

      distance += group.size();

      auto & last_entry = *prev(end(group));

      if (displaced_entry.priority > last_entry.first ) {

        group[displaced_entry.priority] = displaced_entry.tag;  // insert de_entry

        // delete last entry and make it de
        auto last_entry_it = prev(group.end());
        displaced_entry.priority = (*last_entry_it).first;
        displaced_entry.tag = (*last_entry_it).second;
        group.erase(last_entry_it);
      }

      ++it;
    }

  }


  // (address not found)
  // add de to end of last group

  if (stack_.size() == 0) {
    // create a new group
    stack_.push_front(Group());
  }
  auto & last_group = *prev(end(stack_));
  last_group[displaced_entry.priority] = displaced_entry.tag;

  return false;  // cold miss
}


void
FAOPTCache::outputMisses(ostream & out) {
  uint64_t num_misses = total_accesses_;
  out<<"num_lines"<<","<<"num_misses"<<endl;
  out << 0 << "," << num_misses << endl;
  for(auto it=begin(hits_); it!=end(hits_); ++it) {
    num_misses -= (*it).second;
    out<<((*it).first+1)<<","<<num_misses<<endl;
  }
}
